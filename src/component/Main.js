import React from "react";
import Button from "./Button/Button";
import Modal from "./modal/Modal";

export default class Main extends React.Component {
    state = {
        modal1: false,
        modal2: false,
        modal: {
            header: '',
            text: '',
            actions: [],
            open: false,
        }
    };

    closeModal = (e) => {
        if (e.target !== e.currentTarget) {
            return;
        }
        this.setState({
            modal: {
                open: false
            }
        })
    };

    render() {
        const handlerBtn = ({header, text, actions}) => {
            this.setState({
                modal: {
                    header,
                    text,
                    actions,
                    open: true
                }
            });
        };
        const buttonProperty = [
            {id: 1, text: 'Open first modal', backgroundColor: 'green'},
            {id: 2, text: 'Open second modal', backgroundColor: 'blue'}
        ];
        const modalDataFirst =
            {
                header: 'Do you want to delete this file?',
                text: 'Open first modal',
                actions: [
                    <Button text='Yes'/>,
                    <Button text='No'/>
                ]
            };
        const modalDataSecond =
            {
                header: '2Do you want to delete this file?',
                text: '2Open first modal',
                actions: [
                    <Button text='2Yes'/>,
                    <Button text='2No'/>
                ]
            };

        const modal = this.state.modal.open && <Modal
            bgrndClick={this.closeModal}
            header={this.state.modal.header}
            text={this.state.modal.text}
            actions={this.state.modal.actions}
            open={this.state.modal.open}
        />
        return (
            <div>
                <Button text={buttonProperty[0].text}
                    buttoncolor={buttonProperty[0].backgroundColor}
                    handlerBtn={() => handlerBtn(modalDataFirst)}
                />
                <Button text={buttonProperty[1].text}
                    buttoncolor={buttonProperty[1].backgroundColor}
                    handlerBtn={() => handlerBtn(modalDataSecond)}
                />
                {modal}
            </div>
        )
    }
}