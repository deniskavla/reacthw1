import React, {Component} from "react";
import {Fragment} from 'react';
import s from './Modal.module.css';
import PropTypes from 'prop-types';

export default class Modal extends Component {
    constructor(props){
        super(props);
        console.log(this.props);
        this.openStatus = this.props.open ? 'open' : ''
    }

    render() {
        console.log(this.props);
        return (
            <Fragment>
                <div className={`${s.modalOverlay} ${this.props.open ? s.open : ''}`} onClick={this.props.bgrndClick}>
                    <div className={s.modalWindow}>
                        <div className={s.modalHeader}>
                            <div className={s.modalHeaderTitle}>
                                {this.props.header}
                                <div className={s.closed} onClick={this.props.bgrndClick}>+</div>
                            </div>
                        </div>
                        <div className="modalBody">
                            {this.props.text}
                        </div>
                        <div className="modalFooter">
                            {this.props.actions}
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}


Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.string,
    actions: PropTypes.array,
};
