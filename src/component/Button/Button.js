import React from "react";
import PropTypes from 'prop-types';

export default class Button extends React.Component {
    render() {
        return (
            <button type='button'
                style={{backgroundColor: this.props.buttoncolor}}
                onClick={this.props.handlerBtn}
            >{this.props.text}
            </button>
        )
    }
}

Button.propTypes = {
    buttoncolor: PropTypes.string,
    text: PropTypes.string
};